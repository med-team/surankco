/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.surankco.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

import de.rki.ng4.surankco.data.FastqReads;

/**
 * Import of read qualities from Qual/Qua-Files
 * 
 * @author Mathias Kuhring
 *
 */
public class Qual {
	
	public Qual(String filename, FastqReads reads) {
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(filename)));
			try {
				String line;
				String[] splits;
				while ((line = br.readLine()) != null){
					if (line.startsWith(">")){
						splits = line.split(" ");
						reads.readId.add(splits[0].substring(1));
						reads.readQuality.add(new Vector<Integer>());
					}
					else {
						splits = line.split(" ");
						for (String s : splits){
							reads.readQuality.lastElement().add(Integer.parseInt(s));
						}
					}
				}
			}
			finally {
				br.close();
			}
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("error: problems reading file: " + filename);
			System.exit(1);
		} 
		catch (IOException e) {
			e.printStackTrace();
			System.out.println("error: problems reading file: " + filename);
			System.exit(1);
		}
	}
}
