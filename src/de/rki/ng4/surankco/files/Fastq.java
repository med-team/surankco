/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.surankco.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

import de.rki.ng4.surankco.data.FastqReads;
import de.rki.ng4.surankco.feature.Main;

/**
 * Import of read qualities from Fastq-Files
 * 
 * @author Mathias Kuhring
 *
 */
public class Fastq {

	String fastqParameter;
	String fastqVersion;
	
	Vector<String> quality;
	
	public Fastq(String filename, FastqReads reads, String parameter) {
		quality = new Vector<String>();
		
		fastqParameter = parameter;
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(filename)));
			try {
				String line;
				String[] splits;
				while ((line = br.readLine()) != null){
					if (line.startsWith("@")){
						// parse read id
						splits = line.split(" ");
						reads.readId.add(splits[0].substring(1));
						// skip read sequence
						br.readLine();
						// skip +
						br.readLine();
						// read quality
						line = br.readLine();
						quality.add(line);
					}
				}
			}
			finally {
				br.close();
			}
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("error: problems reading file: " + filename);
			System.exit(1);
		} 
		catch (IOException e) {
			e.printStackTrace();
			System.out.println("error: problems reading file: " + filename);
			System.exit(1);
		}
		
		if (Main.DEBUGMODE){
			System.out.println(reads.readId.get(0));
			System.out.println(quality.get(0));
		}
		
		reads.readQuality.addAll(parseFastqQuality());
	}	
	
	// Coordination of Fastq Quality conversion
	private Vector<Vector<Integer>> parseFastqQuality(){
		// convert from ascii to integer
		Vector<Vector<Integer>> intQuality = asciiToNumber();
		// determine fastq version
		fastqVersion = getFastqVersion(new Vector<Vector<Integer>>(intQuality.subList(0, Math.min(intQuality.size(), 10))), fastqParameter);
		// convert to illumina1.8+
		intQuality = convertToIllumina18(intQuality);
		
		return intQuality;
	}
	
	// Convert different fastq quality versions to illumina1.8+
	private Vector<Vector<Integer>> convertToIllumina18(Vector<Vector<Integer>> intQuality) {
		int illumina18Cutoff = 41;
		
		switch (fastqVersion) {
		case "sanger":
			intQuality = shiftQualities(intQuality, 33, illumina18Cutoff);
			break;
		case "solexa":
			intQuality = shiftQualities(intQuality, 64, 62);
			intQuality = convertSolexaToPhred(intQuality);
			intQuality = shiftQualities(intQuality, 0, illumina18Cutoff);
			break;
		case "illumina13":
			intQuality = shiftQualities(intQuality, 64, illumina18Cutoff);
			break;
		case "illumina15":
			intQuality = shiftQualities(intQuality, 64, illumina18Cutoff);
			break;
		case "illumina18":
			intQuality = shiftQualities(intQuality, 33, illumina18Cutoff);
			break;
		default:
			System.out.println("error: bad fastq version");
			System.exit(1);
			break;
		}
		return intQuality;
	}

	// converts Solexa Quality to Phred Quality: qSolexy = -10 x log10(Pe / (1-Pe)) -> qPhred = -10 x log10(Pe)
	// according to Cock et. al 2009, The Sanger FASTQ file format...
	private Vector<Vector<Integer>> convertSolexaToPhred(Vector<Vector<Integer>> intQuality) {
		double qSolexa = 0;
		int converted = 0;
		
		for (int i=0; i<intQuality.size(); i++){
			for (int j=0; j<intQuality.get(i).size(); j++){
				qSolexa = intQuality.get(i).get(j);
				converted = (int) Math.round(10d * Math.log10( Math.pow(10d,(qSolexa/10d)) + 1d ));
				intQuality.get(i).set(j,converted);
			}
		}
		
		return intQuality;
	}

	// Scaling of Qualities
	private Vector<Vector<Integer>> shiftQualities(Vector<Vector<Integer>> intQuality, int shift, int cutoff) {
		int shifted = 0;
		
		for (int i=0; i<intQuality.size(); i++){
			for (int j=0; j<intQuality.get(i).size(); j++){
				shifted = Math.min((intQuality.get(i).get(j) - shift), cutoff);
				intQuality.get(i).set(j,shifted);
			}
		}
		
		return intQuality;
	}

	// Converts the fastq quality ascii strings to integer vectors
	private Vector<Vector<Integer>> asciiToNumber() {
		Vector<Vector<Integer>> intQuality = new Vector<Vector<Integer>>();
		
		for (String read : quality){
			intQuality.add(new Vector<Integer>());
			for (char qual : read.toCharArray()){
				intQuality.lastElement().add((int) qual);
			}
		}
		
		return intQuality;
	}

	// Determines Fastq version, estimating if parameter is "auto"
	private String getFastqVersion(Vector<Vector<Integer>> intQuality, String parameter){
		Vector<String> version = new Vector<String>();
		if (parameter.matches("auto")){
			int minQual = Integer.MAX_VALUE;
			int maxQual = Integer.MIN_VALUE;
			for (Vector<Integer> vec : intQuality){
				for (Integer qual : vec){
					minQual = Math.min(minQual, qual);
					maxQual = Math.max(maxQual, qual);
				}
			}
			
			if (Main.DEBUGMODE){
				System.out.println("min qual (ascii): " + minQual);	
				System.out.println("max qual (ascii): " + maxQual);	
			}
			
			if (minQual >= 33 && maxQual <= 126){
				version.add("sanger");
			}
			if (minQual >= 35 && maxQual <= 74){
				version.add("illumina18");
			}
			if (minQual >= 59 && maxQual <= 126){
				version.add("solexa");
			}
			if (minQual >= 64 && maxQual <= 126){
				version.add("illumina13");
			}
			if (minQual >= 66 && maxQual <= 126){
				version.add("illumina15");
			}
		}
		else{
			version.add(parameter);
		}
		if (version.size() > 1){
			System.out.println("error: fastq version dedection is ambiguous " + version + ".\n       please set version manually!");
			System.exit(1);
		}
		if (version.size() == 0){
			System.out.println("error: fastq version dedection failed, unknown version.\n       please set version manually!");
			System.exit(1);
		}
		if (Main.DEBUGMODE){
			System.out.println("detected fastq version: " + version.get(0));			
		}
		return version.get(0);
	}
		
}