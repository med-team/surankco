package de.rki.ng4.surankco.files;

import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.util.SamLocusIterator;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.rki.ng4.surankco.data.SamContig;

public class Sam {

	public static void main(String[] args){
		Sam test = new Sam("data/ex1.sam");
		for (SamContig contig : test.getContigs().values()){
			System.out.println(contig.getId());
		}
	}

	private HashMap<String,SamContig> contigs;

	public Sam(String filename) {

		contigs = new HashMap<String,SamContig>();

		final SamReader readReader = SamReaderFactory.makeDefault().open(new File(filename));

		//		System.out.println(readReader.getFileHeader());

		String refId;
		for (final SAMRecord samRecord : readReader) {

			if (!samRecord.getReadUnmappedFlag()){
				refId = samRecord.getReferenceName();
				if (!contigs.containsKey(refId)){
					contigs.put(refId, new SamContig(refId));
				}
				contigs.get(refId).addRead(samRecord);
			}

			//			System.out.println(samRecord);
		}

		final SamReader alignmentReader = SamReaderFactory.makeDefault().open(new File(filename));

		for (final SamLocusIterator.LocusInfo locus : new SamLocusIterator(alignmentReader)){
			refId = locus.getSequenceName();

			if (contigs.containsKey(locus.getSequenceName())){
				ArrayList<Byte> alignmentColumn = new ArrayList<Byte>();
				
				List<SamLocusIterator.RecordAndOffset> recs = locus.getRecordAndPositions();

//				System.out.println(refId + "\t" + locus.getPosition() + ": ");
				for (SamLocusIterator.RecordAndOffset rec : recs){
					alignmentColumn.add(rec.getReadBase());
					//				System.out.print((char) rec.getReadBase() + " ");
				}
				//			System.out.println();
				contigs.get(locus.getSequenceName()).addAlignmentColumn(alignmentColumn);
			}
		}
	}

	public HashMap<String,SamContig> getContigs(){
		return contigs;
	}
}
