/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.surankco.tools;

import java.util.Collections;
import java.util.Vector;

public class Statistic {

	public static <N extends Number> double mad(Vector<N> data){
		final double scalefactor = 1.4826;
		double median = median(data);
		Vector<Double> diffs = new Vector<Double>();
		for (N num : data){ diffs.add(Math.abs(num.doubleValue()-median)); }
		return median(diffs) * scalefactor;
	}
	
	public static <N extends Number> Vector<Double> mads(Vector<Vector<N>> data){
		Vector<Double> mads = new Vector<Double>();
		for (Vector<N> vn : data){ mads.add(mad(vn)); }	
		return mads;
	}
	
	public static <N extends Number> double max(Vector<N> data){
		double max = Double.MIN_VALUE;
		for (N num : data){ max = Math.max(max, num.doubleValue()); }
		return max;
	}
	
	public static <N extends Number> Vector<Double> maxs(Vector<Vector<N>> data){
		Vector<Double> maxs = new Vector<Double>();
		for (Vector<N> vn : data){ maxs.add(max(vn)); }	
		return maxs;
	}
	
	public static <N extends Number> double mean(Vector<N> data){
		return sum(data)/data.size();
	}
	
	public static  <N extends Number> Vector<Double> means(Vector<Vector<N>> data){
		Vector<Double> means = new Vector<Double>();
		for (Vector<N> vn : data){ means.add(mean(vn)); }	
		return means;
	}
	
	public static <N extends Number> double median(Vector<N> data){
		Vector<Double> copy = new Vector<Double>();
		for (N num : data){ copy.add(num.doubleValue()); }
		Collections.sort(copy);
		int n = copy.size();
		if (n % 2 == 1){ return copy.get(((n+1)/2) -1); }
		else{ return 0.5 * (copy.get((n/2) -1) + copy.get(((n/2)+1) -1)); }
	}
	
	public static <N extends Number> Vector<Double> medians(Vector<Vector<N>> data){
		Vector<Double> medians = new Vector<Double>();
		for (Vector<N> vn : data){ medians.add(median(vn)); }	
		return medians;
	}
	
	public static <N extends Number> double min(Vector<N> data){
		double min = Double.MAX_VALUE;
		for (N num : data){ min = Math.min(min, num.doubleValue()); }
		return min;
	}
	
	public static <N extends Number> Vector<Double> mins(Vector<Vector<N>> data){
		Vector<Double> mins = new Vector<Double>();
		for (Vector<N> vn : data){ mins.add(min(vn)); }	
		return mins;
	}
	
	public static <N extends Number> double sd(Vector<N> data){
		return Math.sqrt(var(data));
	}
	
	public static <N extends Number> Vector<Double> sds(Vector<Vector<N>> data){
		Vector<Double> sds = new Vector<Double>();
		for (Vector<N> vn : data){ sds.add(sd(vn)); }	
		return sds;
	}
	
	public static <N extends Number> double sum(Vector<N> data){
		double sum = 0;
		for (N num : data){ sum += num.doubleValue(); }
		return sum;
	}
	
	public static <N extends Number> Vector<Double> sums(Vector<Vector<N>> data){
		Vector<Double> sums = new Vector<Double>();
		for (Vector<N> vn : data){ sums.add(sum(vn)); }	
		return sums;
	}
	
	public static <N extends Number> double var(Vector<N> data){
		double var = 0;
		double mean = mean(data);
		for (N num : data){ var += Math.pow((num.doubleValue()-mean), 2); }
		return var/(data.size()-1);
	}
	
	public static <N extends Number> Vector<Double> vars(Vector<Vector<N>> data){
		Vector<Double> vars = new Vector<Double>();
		for (Vector<N> vn : data){ vars.add(var(vn)); }	
		return vars;
	}

}
