/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.surankco.feature;

import java.util.Vector;

import de.rki.ng4.surankco.tools.Statistic;



public class Coverage {

	private Vector<Vector<Integer>> coverages;
	
	private  Vector<Vector<Integer>> coverageLeftEnd;
	private  Vector<Vector<Integer>> coverageRightEnd;
	
	private  Vector<Double> coverageMinEndMean;
	private  Vector<Double> coverageMinEndSD;
	private  Vector<Double> coverageMinEndMedian;
	private  Vector<Double> coverageMinEndMAD;
	private  Vector<Double> coverageMinEndMin;
	private  Vector<Double> coverageMinEndMax;
	
	private  Vector<Double> coverageMaxEndMean;
	private  Vector<Double> coverageMaxEndSD;
	private  Vector<Double> coverageMaxEndMedian;
	private  Vector<Double> coverageMaxEndMAD;
	private  Vector<Double> coverageMaxEndMin;
	private  Vector<Double> coverageMaxEndMax;
	
	boolean calced = false;
	
	public Coverage(Vector<Vector<Integer>> coverages){
		this.coverages = coverages;
	}
	
	public boolean calcEndCovs(Vector<Double> endSize){
		boolean sizeCheck = endSize.size() == coverages.size();
		if(sizeCheck){
			coverageLeftEnd = new Vector<Vector<Integer>>();
			coverageRightEnd = new Vector<Vector<Integer>>();
			for(int i=0; i<coverages.size(); i++){
				coverageLeftEnd.add(new Vector<Integer>(coverages.get(i).subList(0, Math.min(endSize.get(i).intValue(), coverages.get(i).size()))));
				coverageRightEnd.add(new Vector<Integer>(coverages.get(i).subList(Math.max(0, coverages.get(i).size()-endSize.get(i).intValue()), coverages.get(i).size())));
			}
		}
		calcStats();
		return sizeCheck;
	}
		
	private void calcStats(){
		coverageMinEndMean = new Vector<Double>();
		coverageMinEndSD = new Vector<Double>();
		coverageMinEndMedian = new Vector<Double>();
		coverageMinEndMAD = new Vector<Double>();
		coverageMinEndMin = new Vector<Double>();
		coverageMinEndMax = new Vector<Double>();
		
		coverageMaxEndMean = new Vector<Double>();
		coverageMaxEndSD = new Vector<Double>();
		coverageMaxEndMedian = new Vector<Double>();
		coverageMaxEndMAD = new Vector<Double>();
		coverageMaxEndMin = new Vector<Double>();
		coverageMaxEndMax = new Vector<Double>();
		
		Vector<Double> leftMean = Statistic.means(coverageLeftEnd);
		Vector<Double> leftSD = Statistic.sds(coverageLeftEnd);
		Vector<Double> leftMedian = Statistic.medians(coverageLeftEnd);
		Vector<Double> leftMAD = Statistic.mads(coverageLeftEnd);
		Vector<Double> leftMin = Statistic.mins(coverageLeftEnd);
		Vector<Double> leftMax = Statistic.maxs(coverageLeftEnd);
		
		Vector<Double> rightMean = Statistic.means(coverageRightEnd);
		Vector<Double> rightSD = Statistic.sds(coverageRightEnd);
		Vector<Double> rightMedian = Statistic.medians(coverageRightEnd);
		Vector<Double> rightMAD = Statistic.mads(coverageRightEnd);
		Vector<Double> rightMin = Statistic.mins(coverageRightEnd);
		Vector<Double> rightMax = Statistic.maxs(coverageRightEnd);
		
		for (int i=0; i<leftMean.size(); i++){
			if (leftMean.get(i) <= rightMean.get(i)){
				coverageMinEndMean.add(leftMean.get(i));
				coverageMinEndSD.add(leftSD.get(i));
				coverageMinEndMedian.add(leftMedian.get(i));
				coverageMinEndMAD.add(leftMAD.get(i));
				coverageMinEndMin.add(leftMin.get(i));
				coverageMinEndMax.add(leftMax.get(i));
				
				coverageMaxEndMean.add(rightMean.get(i));
				coverageMaxEndSD.add(rightSD.get(i));
				coverageMaxEndMedian.add(rightMedian.get(i));
				coverageMaxEndMAD.add(rightMAD.get(i));
				coverageMaxEndMin.add(rightMin.get(i));
				coverageMaxEndMax.add(rightMax.get(i));
			}
			else{
				coverageMinEndMean.add(rightMean.get(i));
				coverageMinEndSD.add(rightSD.get(i));
				coverageMinEndMedian.add(rightMedian.get(i));
				coverageMinEndMAD.add(rightMAD.get(i));
				coverageMinEndMin.add(rightMin.get(i));
				coverageMinEndMax.add(rightMax.get(i));
				
				coverageMaxEndMean.add(leftMean.get(i));
				coverageMaxEndSD.add(leftSD.get(i));
				coverageMaxEndMedian.add(leftMedian.get(i));
				coverageMaxEndMAD.add(leftMAD.get(i));
				coverageMaxEndMin.add(leftMin.get(i));
				coverageMaxEndMax.add(leftMax.get(i));
			}
		}
		calced = true;
	}
	
	public Vector<Double> getCoverageMinEndMean(){
		return coverageMinEndMean;
	}
	
	public Vector<Double> getCoverageMinEndSD(){
		return coverageMinEndSD;
	}
	
	public Vector<Double> getCoverageMinEndMedian(){
		return coverageMinEndMedian;
	}
	
	public Vector<Double> getCoverageMinEndMAD(){
		return coverageMinEndMAD;
	}
	
	public Vector<Double> getCoverageMinEndMin(){
		return coverageMinEndMin;
	}
	
	public Vector<Double> getCoverageMinEndMax(){
		return coverageMinEndMax;
	}
	
	public Vector<Double> getCoverageMaxEndMean(){
		return coverageMaxEndMean;
	}
	
	public Vector<Double> getCoverageMaxEndSD(){
		return coverageMaxEndSD;
	}
	
	public Vector<Double> getCoverageMaxEndMedian(){
		return coverageMaxEndMedian;
	}
	
	public Vector<Double> getCoverageMaxEndMAD(){
		return coverageMaxEndMAD;
	}
	
	public Vector<Double> getCoverageMaxEndMin(){
		return coverageMaxEndMin;
	}
	
	public Vector<Double> getCoverageMaxEndMax(){
		return coverageMaxEndMax;
	}
}
