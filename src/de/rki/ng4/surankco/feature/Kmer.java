/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.surankco.feature;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Vector;

public class Kmer {

	private Vector<String> consensusSequences;
	private int kmerSize;
	
	private Vector<Integer> kmerEndUniquenessMin;
	private Vector<Integer> kmerEndUniquenessMax;
	
	public Kmer(Vector<String> conSeqs, int kmerSize){
		this.kmerSize = kmerSize;
		consensusSequences = new Vector<String>();
		StringBuffer temp;
		for (String s : conSeqs) {
			temp = new StringBuffer();
			for (Character ch : s.toUpperCase().toCharArray()){
				if (ch == 'A' || ch == 'C' || ch == 'G' || ch == 'T'){
					temp.append(ch);
				}
			}
			consensusSequences.add(temp.toString());
		}
	}
	
	public Vector<Integer> getKmerGlobalUniqueness(){
		Vector<HashMap<KmerKey,Boolean>> kmers = getKmers(kmerSize, consensusSequences);
		Vector<Integer> uniqueKmerCount = new Vector<Integer>();
		boolean unique;
		int count;
		for (int i=0; i<kmers.size(); i++){
			count = 0;
			for (KmerKey kmer : kmers.get(i).keySet()){
				unique = true;
				for (int j=0; j<kmers.size(); j++){
					if (i!=j){
						if (kmers.get(j).containsKey(kmer)){
							unique = false;
							break;
						}
					}
				}
				if (unique){
					count++;
				}
			}
			uniqueKmerCount.add(count);
		}
		return uniqueKmerCount;
	}
	
	public Vector<Integer> getKmerEndUniquenessMin(int endLength){
		if (kmerEndUniquenessMin == null)
			calcKmerEndUniqueness(kmerSize, endLength);
		return kmerEndUniquenessMin;
	}
	
	public Vector<Integer> getKmerEndUniquenessMax(int endLength){
		if (kmerEndUniquenessMax == null)
			calcKmerEndUniqueness(kmerSize, endLength);
		return kmerEndUniquenessMax;
	}
	
	public Vector<Integer> getKmerSize(){
		Vector<Integer> kmerSizes = new Vector<Integer>();
		for (int i=0; i<consensusSequences.size(); i++){
			kmerSizes.add(kmerSize);
		}
		return kmerSizes;
	}
	
	private void calcKmerEndUniqueness(int kmerSize, int endLength){
		Vector<HashMap<KmerKey,Boolean>> kmersLeft = getKmers(kmerSize, sequencePrefixes(endLength));
		Vector<HashMap<KmerKey,Boolean>> kmersRight = getKmers(kmerSize, sequenceSuffixes(endLength));
		kmerEndUniquenessMin = new Vector<Integer>();
		kmerEndUniquenessMax = new Vector<Integer>();
		int left, right;
		boolean unique;
		for (int i=0; i<kmersLeft.size(); i++){
			left = 0;
			for (KmerKey kmer : kmersLeft.get(i).keySet()){
				unique = true;
				for (int j=0; j<kmersLeft.size(); j++){
					if (i!=j){
						if (kmersLeft.get(j).containsKey(kmer) || kmersRight.get(j).containsKey(kmer)){
							unique = false;
							break;
						}
					}
				}
				if (unique){
					left++;
				}
			}
			right = 0;
			for (KmerKey kmer : kmersRight.get(i).keySet()){
				unique = true;
				for (int j=0; j<kmersRight.size(); j++){
					if (i!=j){
						if (kmersLeft.get(j).containsKey(kmer) || kmersRight.get(j).containsKey(kmer)){
							unique = false;
							break;
						}
					}
				}
				if (unique){
					right++;
				}
			}
			kmerEndUniquenessMax.add(Math.max(left, right));
			kmerEndUniquenessMin.add(Math.min(left, right));
		}
	}
	
	private Vector<HashMap<KmerKey, Boolean>> getKmers(int kmerSize, Vector<String> sequences){		
		Vector<HashMap<KmerKey,Boolean>> kmers = new Vector<HashMap<KmerKey,Boolean>>();
		for (String seq : sequences){
			kmers.add(new HashMap<KmerKey, Boolean>());
			String kmer;
			for (int i=0; i<=seq.length()-kmerSize; i++){
				kmer = seq.substring(i, i+kmerSize);
				kmers.lastElement().put(new KmerKey(kmer), true);
				
			}
		}
		return kmers;
	}

	public Vector<Vector<Double>> getKmerDistances(){
		Vector<HashMap<KmerKey, Boolean>> kmers = getKmers(kmerSize, consensusSequences);
		Vector<Vector<Double>> pairwiseDistances = new Vector<Vector<Double>>();
		for (HashMap<KmerKey, Boolean> kmerMapA : kmers){
			pairwiseDistances.add(new Vector<Double>());
			for (HashMap<KmerKey, Boolean> kmerMapB : kmers){
				if (!kmerMapA.equals(kmerMapB))
					pairwiseDistances.lastElement().add(editDistance(kmerMapA,kmerMapB));
			}
		}
		return pairwiseDistances;
	}

	private double editDistance(HashMap<KmerKey, Boolean> kmerMapA, HashMap<KmerKey, Boolean> kmerMapB){
		double distance = 0;
		double sizeA = kmerMapA.size();
		double sizeB = 0;	
		for (Entry<KmerKey, Boolean> entry : kmerMapA.entrySet()){
			if (kmerMapB.containsKey(entry.getKey())) sizeB++;
		}
		for (Entry<KmerKey, Boolean> entry : kmerMapA.entrySet()){
			if (kmerMapB.containsKey(entry.getKey()))
				distance += Math.abs((1/sizeA) - (1/sizeB));
			else
				distance += (1/sizeA);
		}
		return distance;
	}

	private Vector<String> sequencePrefixes(int length){
		Vector<String> prefixes = new Vector<String>();
		for (String s : consensusSequences){
			prefixes.add(s.substring(0, Math.min(length, s.length())));
		}
		return prefixes;
	}

	private Vector<String> sequenceSuffixes(int length){
		Vector<String> suffixes = new Vector<String>();
		for (String s : consensusSequences){
			suffixes.add(s.substring(Math.max(0, s.length()-length), s.length()));
		}
		return suffixes;
	}

	class MutableInt {
		private int value = 0;
		public void inc () { ++value; }
		public int get () { return value; }
		public String toString(){
			return Integer.toString(value);
		}
	}
	
	private class KmerKey{
		private String kmer;
		private String revcomp;
		
		private int hash;

		public KmerKey(String kmer){
			this.kmer = kmer.toUpperCase();
			this.revcomp = reverseCompl(this.kmer);
			initHash();
		}
		
		@Override
		public String toString(){
			return kmer + "<>" + revcomp;
		}
		
		private String reverseCompl(String kmer){
			StringBuffer revcomp = new StringBuffer();
			for (Character ch : kmer.toCharArray()){
				switch (ch){
				case 'A':
					revcomp.append('T');
					break;
				case 'C':
					revcomp.append('G');
					break;
				case 'G':
					revcomp.append('C');
					break;
				case 'T':
					revcomp.append('A');
					break;
				default:
					revcomp.append(ch);				
				}
			}
			return revcomp.reverse().toString();
		}
		
		@Override
		public boolean equals(Object obj){
			if(obj == this) {
				return true;
			}
			if(!(obj instanceof KmerKey)) {
				return false;
			}
			
			KmerKey kk = (KmerKey) obj;
			return (this.kmer.matches(kk.kmer) || this.kmer.matches(kk.revcomp));
		}
		
		@Override
		public int hashCode(){
			return hash;
		}
		
		private void initHash(){
			hash = Math.max(hash(kmer), hash(revcomp));
		}
		
		private int hash(String s){
			int hash = 0;
			for (Character ch : s.toCharArray()){
				switch (ch){
				case 'A':
					hash = hash << 2;
					hash = hash | 0;
					break;
				case 'C':
					hash = hash << 2;
					hash = hash | 1;
					break;
				case 'G':
					hash = hash << 2;
					hash = hash | 2;
					break;
				case 'T':
					hash = hash << 2;
					hash = hash | 3;
					break;
				}
			}
			return hash;
		}
	}
}
