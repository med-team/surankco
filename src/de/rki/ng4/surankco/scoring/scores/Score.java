/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.surankco.scoring.scores;

import de.rki.ng4.surankco.scoring.PSLHit;

public abstract class Score {

	public abstract String getScoreName();
	public abstract double calculate(PSLHit hit);

}
