/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.surankco.scoring;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Vector;


public class PSLPrettyParser {

	private String fileName;
	private Vector<PSLHit> hits;
	
	public PSLPrettyParser(String fileName){
		this.fileName = fileName;
		hits = new Vector<PSLHit>();	
		
		try {
			LineNumberReader reader = new LineNumberReader(new FileReader(new File(fileName)));
			try {
				String line;

				while ((line = reader.readLine()) != null){
					if (line.startsWith(">")){
						hits.add(new PSLHit(line));
					}
					else {
						hits.lastElement().appendAlignment(line);
					}
				}
				
				for (PSLHit hit : hits){
					hit.parseAlignment();
				}
				
			}
			finally {
				reader.close();
			}
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("error: problems reading file: " + fileName);
			System.exit(1);
		} 
		catch (IOException e) {
			e.printStackTrace();
			System.out.println("error: problems reading file: " + fileName);
			System.exit(1);
		}
	}
	
	public void printPrettyPslHits(){
		for (PSLHit hit : hits){
			System.out.print(hit.toString());
		}
	}
	
	public void export(String filename){
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(new File(filename)));
			
			try {
				for (PSLHit hit : hits){
					writer.write(hit.toString());
					writer.newLine();
				}
			}
			finally {
				writer.close();
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("error: problems writing file: " + fileName);
			System.exit(1);
		}
	}

	public Vector<PSLHit> getHits() {
		return hits;
	}
	
}