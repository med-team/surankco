package de.rki.ng4.surankco.data;

import htsjdk.samtools.SAMRecord;

import java.util.ArrayList;

public class SamContig {

	private String id;
	
	private ArrayList<SAMRecord> reads;
	private ArrayList<ArrayList<Byte>> alignment;
	
	public SamContig(String id){
		this.id = id;
		reads = new ArrayList<SAMRecord>();
		alignment = new ArrayList<ArrayList<Byte>>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ArrayList<SAMRecord> getReads() {
		return reads;
	}

	public void addRead(SAMRecord read) {
		this.reads.add(read);
	}

	public ArrayList<ArrayList<Byte>> getAlignment() {
		return alignment;
	}

	public void addAlignmentColumn(ArrayList<Byte> alignmentColumn) {
		this.alignment.add(alignmentColumn);
	}
}
