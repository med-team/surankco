/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.surankco.data;

import java.util.Vector;

public class AceContig{
	
	private int[][] nucleotides;
	
	private int lineCO;
	private int lineBQ;

	private String header;

	private String id;
	private int baseCount;
	private int readCount;
	private int baseSeqmentCount;
	private String orientation;

	private String consensusSequence;

	private Vector<Integer> qualityValues;

	private Vector<AceRead> reads;

	public AceContig(String header){
		// CO 1 30502 510 273 U
		this.header = header;
		
		String[] splits = header.split(" ");
		
		id = splits[1];
		baseCount = Integer.parseInt(splits[2]);
		readCount = Integer.parseInt(splits[3]);
		baseSeqmentCount = Integer.parseInt(splits[4]);
		orientation = splits[5];
		
		qualityValues = new Vector<Integer>();
		reads = new Vector<AceRead>();
	}
	
	public void addRead(AceRead read){
		reads.add(read);
	}
	

	/**
	 * @return the nucleotides
	 */
	public int[][] getNucleotides() {
		return nucleotides;
	}

	/**
	 * @param nucleotides the nucleotides to set
	 */
	public void setNucleotides(int[][] nucleotides) {
		this.nucleotides = nucleotides;
	}

	/**
	 * @return the lineCO
	 */
	public int getLineCO() {
		return lineCO;
	}

	/**
	 * @param lineCO the lineCO to set
	 */
	public void setLineCO(int lineCO) {
		this.lineCO = lineCO;
	}

	/**
	 * @return the lineBQ
	 */
	public int getLineBQ() {
		return lineBQ;
	}

	/**
	 * @param lineBQ the lineBQ to set
	 */
	public void setLineBQ(int lineBQ) {
		this.lineBQ = lineBQ;
	}

	/**
	 * @return the header
	 */
	public String getHeader() {
		return header;
	}

	/**
	 * @param header the header to set
	 */
	public void setHeader(String header) {
		this.header = header;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the baseCount
	 */
	public int getBaseCount() {
		return baseCount;
	}

	/**
	 * @param baseCount the baseCount to set
	 */
	public void setBaseCount(int baseCount) {
		this.baseCount = baseCount;
	}

	/**
	 * @return the readCount
	 */
	public int getReadCount() {
		return readCount;
	}

	/**
	 * @param readCount the readCount to set
	 */
	public void setReadCount(int readCount) {
		this.readCount = readCount;
	}

	/**
	 * @return the baseSeqmentCount
	 */
	public int getBaseSeqmentCount() {
		return baseSeqmentCount;
	}

	/**
	 * @param baseSeqmentCount the baseSeqmentCount to set
	 */
	public void setBaseSeqmentCount(int baseSeqmentCount) {
		this.baseSeqmentCount = baseSeqmentCount;
	}

	/**
	 * @return the orientation
	 */
	public String getOrientation() {
		return orientation;
	}

	/**
	 * @param orientation the orientation to set
	 */
	public void setOrientation(String orientation) {
		this.orientation = orientation;
	}

	/**
	 * @return the consensusSequence
	 */
	public String getConsensusSequence() {
		return consensusSequence;
	}

	/**
	 * @param consensusSequence the consensusSequence to set
	 */
	public void setConsensusSequence(String consensusSequence) {
		this.consensusSequence = consensusSequence;
	}

	/**
	 * @return the qualityValues
	 */
	public Vector<Integer> getQualityValues() {
		return qualityValues;
	}

	/**
	 * @param qualityValues the qualityValues to set
	 */
	public void setQualityValues(Vector<Integer> qualityValues) {
		this.qualityValues = qualityValues;
	}

	/**
	 * @return the reads
	 */
	public Vector<AceRead> getReads() {
		return reads;
	}

	/**
	 * @param reads the reads to set
	 */
	public void setReads(Vector<AceRead> reads) {
		this.reads = reads;
	}
	
}
