#!/usr/bin/Rscript

# Copyright (c) 2014,
# Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
# All rights reserved. For details, please note the license.txt.

# surankco-prediction:  prediction of scores and ranking of contigs using 
#                       trained random forests (from surankco-training) and 
#                       contig features (from surankco-feature)


# get script path
args <- commandArgs(trailingOnly = FALSE)
script.arg <- "--file="
script.name <- sub(script.arg, "", args[grep(script.arg, args)])
script.path <- dirname(script.name)


# testing/debugging
# args <- c("--features=data/adenoABC.features.txt", "-r", "surankco_RFs.RData")
# script.path <- getwd()
DEBUGGING <- FALSE


# sources and libraries
source(paste(script.path, '/r/parameter.R', sep=""))
source(paste(script.path, '/r/import.R', sep=""))
source(paste(script.path, '/r/rf.R', sep=""))
source(paste(script.path, '/r/voting.R', sep=""))
# ...
loadPackages(c("optparse","randomForest"), quietly=TRUE)


# parsing parameter
cat("prepare files\n")
parameters <- parseSurankcoPrediction()

if (DEBUGGING){
  print(args)
  print(parameters)
  print(parameters$files.features)
  print(parameters$files.rf)
}

# import feature files and RFs
cat("import features and RFs\n")
features <- read.table(file=parameters$files.features, sep="\t", 
                       dec = ".", header=TRUE, 
                       colClasses=get.colClasses(parameters$files.features, 3))
rf.loaded <- load(parameters$files.rf)
if (!all(c("rfs") %in% rf.loaded)){
  print("error")
}

# prepare datacat
cat("prepare data\n")
features <- dataFilter(features)
metadata <- features[ ,1:3]
features <- features[ ,-c(1:3)]

# prediction
cat("predict scores\n")
prediction <- scorePrediction(rfs=rfs, input=features, type="response")
weighting <- scorePrediction(rfs=rfs, input=features, type="prob")

# voting and ranking
cat("vote and rank\n")
final.scores <- weightedVotingSum(scoreList=prediction, weightList=weighting)
results <- cbind(metadata, SurankcoContigScore=final.scores)
results <- results[order(results$SurankcoContigScore, decreasing=TRUE),]

# export rankings
cat("export ranking\n")
write.table(results, file=parameters$output.filename,
            sep="\t", dec = ".", col.names=TRUE, row.names=FALSE)

# done
cat("surankco-prediction calculations done\n")